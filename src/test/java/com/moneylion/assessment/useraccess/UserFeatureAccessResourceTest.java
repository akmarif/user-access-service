package com.moneylion.assessment.useraccess;

import com.moneylion.assessment.rest.model.UserFeatureAccessResponse;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/11/2020 */
// @RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserFeatureAccessResourceTest {

  @Autowired private TestRestTemplate restTemplate;

  @Test()
  @Order(1)
  public void getUserFeatureAccess() throws Exception {
    ResponseEntity<UserFeatureAccessResponse> entity =
        this.restTemplate.getForEntity(
            "/feature?email=akmarif@gmail.com&featureName=edit", UserFeatureAccessResponse.class);
    assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(entity.getBody().getCanAccess()).isEqualTo(true);
  }

  @Test
  @Order(2)
  public void updateUserFeatureAccessWithoutEtag() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> request =
        new HttpEntity<String>(
            "{\"featureName\":\"edit\",\"email\":\"akmarif@gmail.com\",\"enable\":true}", headers);

    ResponseEntity entity = this.restTemplate.postForEntity("/feature", request, String.class);
    assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.PRECONDITION_FAILED);
  }

  @Test
  @Order(3)
  public void updateUserFeatureAccessWithSame() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.add("if-match", "*");
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> request =
        new HttpEntity<String>(
            "{\"featureName\":\"edit\",\"email\":\"akmarif@gmail.com\",\"enable\":true}", headers);

    ResponseEntity entity = this.restTemplate.postForEntity("/feature", request, String.class);
    assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NOT_MODIFIED);
  }

  @Test
  @Order(4)
  public void updateUserFeatureAccessWithFalseValue() throws Exception {
    HttpHeaders headers = new HttpHeaders();
    headers.add("if-match", "*");
    headers.setContentType(MediaType.APPLICATION_JSON);

    HttpEntity<String> request =
        new HttpEntity<String>(
            "{\"featureName\":\"edit\",\"email\":\"akmarif@gmail.com\",\"enable\":false}", headers);

    ResponseEntity entity = this.restTemplate.postForEntity("/feature", request, String.class);
    assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
  }

  @Test()
  @Order(5)
  public void getUpdatedUserFeatureAccess() throws Exception {
    ResponseEntity<UserFeatureAccessResponse> entity =
        this.restTemplate.getForEntity(
            "/feature?email=akmarif@gmail.com&featureName=edit", UserFeatureAccessResponse.class);
    assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
    assertThat(entity.getBody().getCanAccess()).isEqualTo(false);
  }
}
