package com.moneylion.assessment.db.entity;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/9/2020 */
public class UserFeatureAccessEntity {
  private String featureName;
  private String email;
  private int hasAccess;
  private String version;

  public UserFeatureAccessEntity(String email, String feature, int hasAccess, String version) {
    this.email = email;
    this.featureName = feature;
    this.hasAccess = hasAccess;
    this.version = version;
  }

  public String getFeatureName() {
    return featureName;
  }

  public void setFeatureName(String featureName) {
    this.featureName = featureName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getHasAccess() {
    return hasAccess;
  }

  public void setHasAccess(int hasAccess) {
    this.hasAccess = hasAccess;
  }

  public String getVersion() {
    return version;
  }

  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(UserFeatureAccessEntity.class.getName())
        .append('@')
        .append(Integer.toHexString(System.identityHashCode(this)))
        .append('[');
    sb.append("hasAccess");
    sb.append('=');
    sb.append(this.hasAccess);
    sb.append(',');
    sb.append("featureName");
    sb.append('=');
    sb.append(this.featureName == null ? "<null>" : this.featureName);
    sb.append(',');
    sb.append("email");
    sb.append('=');
    sb.append(this.email == null ? "<null>" : this.email);
    sb.append(',');
    sb.append("version");
    sb.append('=');
    sb.append(this.version == null ? "<null>" : this.version);
    sb.append(',');
    if (sb.charAt(sb.length() - 1) == ',') {
      sb.setCharAt(sb.length() - 1, ']');
    } else {
      sb.append(']');
    }

    return sb.toString();
  }

  public int hashCode() {
    int result = 1 * 31 + (this.featureName == null ? 0 : this.featureName.hashCode());
    result = result * 31 + (this.email == null ? 0 : this.email.hashCode());
    result = result * 31 + (this.hasAccess);
    result = result * 31 + (this.version == null ? 0 : this.version.hashCode());
    return result;
  }

  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof UserFeatureAccessEntity)) {
      return false;
    } else {
      UserFeatureAccessEntity rhs = (UserFeatureAccessEntity) other;
      return (this.featureName == rhs.featureName
              || this.featureName != null && this.featureName.equals(rhs.featureName))
          && (this.email == rhs.email || this.email != null && this.email.equals(rhs.email))
          && (this.hasAccess == rhs.hasAccess);
    }
  }
}
