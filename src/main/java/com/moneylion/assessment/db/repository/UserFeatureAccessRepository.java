package com.moneylion.assessment.db.repository;

import com.moneylion.assessment.db.entity.UserFeatureAccessEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.Instant;
import java.util.List;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/9/2020 */

/** This class is used to CRUD operation on database. */
@Repository
public class UserFeatureAccessRepository {
  private static final Logger logger = LoggerFactory.getLogger(UserFeatureAccessRepository.class);

  @Autowired JdbcTemplate jdbcTemplate;

  /**
   * This method is used to fetch user feature access object by email and feature
   *
   * @param pEmail
   * @param pFeature
   * @return return {@link UserFeatureAccessEntity} if available otherwise null
   */
  public UserFeatureAccessEntity findByEmailAndFeature(String pEmail, String pFeature) {
    List<UserFeatureAccessEntity> entityList =
        jdbcTemplate.query(
            "select * from USER_FEATURE_ACCESS where email=? and feature_name=?",
            new Object[] {pEmail, pFeature},
            new RowMapper<UserFeatureAccessEntity>() {
              @Override
              public UserFeatureAccessEntity mapRow(ResultSet resultSet, int i)
                  throws SQLException {

                return new UserFeatureAccessEntity(
                    resultSet.getString("email"),
                    resultSet.getString("feature_name"),
                    resultSet.getInt("has_access"),
                    resultSet.getString("version"));
              }
            });

    if (entityList.isEmpty() || entityList.size() > 1) {
      logger.info("user feature access entry count:" + entityList.size() + ". Returning null");
      return null;
    }

    return entityList.get(0);
  }

  /**
   * This method is used to update user feature access option
   *
   * @param pEmail
   * @param pFeature
   * @return return true if update success otherwise false.
   */
  public boolean updateUserFeatureAccess(String pEmail, String pFeature, boolean pEnable) {
    int row =
        jdbcTemplate.update(
            "update USER_FEATURE_ACCESS set has_access=?, version=? where email=? and feature_name=?",
            new Object[] {
              pEnable ? 1 : 0, String.valueOf(Instant.now().toEpochMilli()), pEmail, pFeature
            });
    return row == 1 ? true : false;
  }
}
