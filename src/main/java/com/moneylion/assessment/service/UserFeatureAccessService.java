package com.moneylion.assessment.service;

import com.moneylion.assessment.db.entity.UserFeatureAccessEntity;
import com.moneylion.assessment.db.repository.UserFeatureAccessRepository;
import com.moneylion.assessment.rest.model.UserFeatureModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/9/2020 */

/**
 * This is service class of user feature access which is used for business logic related to use
 * feature and access.
 */
@Service
public class UserFeatureAccessService {
  private static final Logger logger = LoggerFactory.getLogger(UserFeatureAccessService.class);

  @Autowired private UserFeatureAccessRepository userFeatureAccessRepository;

  /**
   * This method is used to get UserFeatureModel by email and feature name
   *
   * @param pEmail
   * @param pFeature
   * @return {@link UserFeatureModel}
   */
  public UserFeatureModel getUserFeatureAccess(String pEmail, String pFeature) {
    // caching mechanism should be here before look into DB.
    UserFeatureAccessEntity userFeature =
        userFeatureAccessRepository.findByEmailAndFeature(pEmail, pFeature);

    logger.info(String.format("UserFeatureAccessEntity: %s", userFeature));
    UserFeatureModel userFeatureModel = null;
    if (userFeature != null) {
      userFeatureModel = new UserFeatureModel(userFeature);
    }
    return userFeatureModel;
  }

  /**
   * Update enable property by email and feature name.
   *
   * @param pEmail
   * @param pFeature
   * @param pEnable
   * @return true if update success otherwise false.
   */
  public boolean updateUserFeatureAccess(String pEmail, String pFeature, boolean pEnable) {
    boolean success =
        userFeatureAccessRepository.updateUserFeatureAccess(pEmail, pFeature, pEnable);
    logger.info(
        String.format(
            "Update request for email: %s, feature: %s, enable: %b UPDATE SUCCESS: %b",
            pEmail, pFeature, pEnable, success));
    return success;
  }
}
