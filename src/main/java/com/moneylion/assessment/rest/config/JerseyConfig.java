package com.moneylion.assessment.rest.config;

import com.moneylion.assessment.rest.api.UserFeatureAccessResource;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/9/2020 */
@Component
public class JerseyConfig extends ResourceConfig {
  @PostConstruct
  public void init() {
    register(UserFeatureAccessResource.class);
  }
}
