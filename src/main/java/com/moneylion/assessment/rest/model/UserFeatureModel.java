package com.moneylion.assessment.rest.model;

import com.moneylion.assessment.db.entity.UserFeatureAccessEntity;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/10/2020 */
public class UserFeatureModel extends AbstractModel {
  private String email;
  private String feature;
  private boolean hasAccess = false;

  public UserFeatureModel(UserFeatureAccessEntity userFeatureAccessEntity) {
    email = userFeatureAccessEntity.getEmail();
    feature = userFeatureAccessEntity.getFeatureName();
    hasAccess = userFeatureAccessEntity.getHasAccess() == 0 ? false : true;
    version = userFeatureAccessEntity.getVersion();
  }

  public UserFeatureModel(String pEmail, String pFeature, boolean pHasAccess) {
    email = pEmail;
    feature = pFeature;
    hasAccess = pHasAccess;
  }

  public String getEmail() {
    return email;
  }

  public String getFeature() {
    return feature;
  }

  public boolean hasAccess() {
    return hasAccess;
  }

  @Override
  public boolean equals(Object other) {
    if (other == this) {
      return true;
    } else if (!(other instanceof UserFeatureModel)) {
      return false;
    } else {
      UserFeatureModel rhs = (UserFeatureModel) other;
      return (this.feature == rhs.feature
              || this.feature != null && this.feature.equals(rhs.feature))
          && (this.email == rhs.email || this.email != null && this.email.equals(rhs.email))
          && (this.hasAccess == rhs.hasAccess);
    }
  }
}
