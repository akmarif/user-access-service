package com.moneylion.assessment.rest.model;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/11/2020 */
public abstract class AbstractModel {
  String version;

  public String getVersion() {
    return version;
  }
}
