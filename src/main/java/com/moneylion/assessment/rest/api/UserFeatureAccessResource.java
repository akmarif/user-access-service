package com.moneylion.assessment.rest.api;

import com.moneylion.assessment.rest.model.AbstractModel;
import com.moneylion.assessment.rest.model.UserFeatureAccessRequest;
import com.moneylion.assessment.rest.model.UserFeatureAccessResponse;
import com.moneylion.assessment.rest.model.UserFeatureModel;
import com.moneylion.assessment.service.UserFeatureAccessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/9/2020 */
// This class is used as a API gateway of user features

@Component
@Path("/feature")
public class UserFeatureAccessResource extends AbstractResource {
  private static final Logger logger = LoggerFactory.getLogger(UserFeatureAccessResource.class);

  @Autowired UserFeatureAccessService userFeatureAccessService;
  @Autowired private HttpServletRequest request;

  /**
   * this endpoint is used to get the user feature enable state.
   *
   * @param pEmail
   * @param pFeatureName
   * @return return json object with canAccess property
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getUserFeatureAccess(
      @QueryParam("email") String pEmail, @QueryParam("featureName") String pFeatureName) {
    logger.info(String.format("Request param email: %s, featureName:%s", pEmail, pFeatureName));
    UserFeatureModel userFeature =
        userFeatureAccessService.getUserFeatureAccess(pEmail, pFeatureName);

    UserFeatureAccessResponse userFeatureAccessResponse = new UserFeatureAccessResponse();
    userFeatureAccessResponse.setCanAccess(userFeature != null ? userFeature.hasAccess() : false);
    Response.ResponseBuilder response = Response.ok(userFeatureAccessResponse);
    if (userFeature != null) {
      // Should return 'no content' if there is not user with provided feature instead of return
      // false by default.
      response = response.tag(userFeature.getVersion());
    }
    return response.build();
  }

  /**
   * This endpoint is used to update a user feature access option. It will return not modified if
   * the current option is same as edited one.
   *
   * @param pUserFeatureAccess
   * @return Response return code
   *     <ul>
   *       <li>412(pre-condition fail) if if-match header is missing or not *.
   *       <li>304(Not Modified) if both edited value and existing value is same. Or no update
   *       <li>200(Ok) Successful update.
   *     </ul>
   */
  @POST // I would use PUT here as this is an update.
  @Consumes({MediaType.APPLICATION_JSON})
  public Response updateUserFeatureAccess(UserFeatureAccessRequest pUserFeatureAccess) {
    logger.info(String.format("Request body: %s", pUserFeatureAccess));

    AbstractModel userFeature =
        userFeatureAccessService.getUserFeatureAccess(
            pUserFeatureAccess.getEmail(), pUserFeatureAccess.getFeatureName());

    if (checkPreCondition(request, userFeature)) {
      return Response.status(412).build();
    }

    if (new UserFeatureModel(
            pUserFeatureAccess.getEmail(),
            pUserFeatureAccess.getFeatureName(),
            pUserFeatureAccess.getEnable())
        .equals(userFeature)) {
      return Response.notModified().build();
    }

    boolean updated =
        userFeatureAccessService.updateUserFeatureAccess(
            pUserFeatureAccess.getEmail(),
            pUserFeatureAccess.getFeatureName(),
            pUserFeatureAccess.getEnable());

    return updated ? Response.ok().build() : Response.notModified().build();
  }
}
