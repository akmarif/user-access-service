package com.moneylion.assessment.rest.api;

import com.moneylion.assessment.rest.model.AbstractModel;

import javax.servlet.http.HttpServletRequest;

/** Author: ariful.haq <ariful.haq@banglalink.net> Date: 6/11/2020 */
public class AbstractResource {

  /**
   * validate etag with model version.
   *
   * @param pRequest
   * @param model
   * @return true if precondition fail otherwise false.
   */
  boolean checkPreCondition(HttpServletRequest pRequest, AbstractModel model) {
    String etagHeader = pRequest.getHeader("if-match");

    if (!"*".equals(etagHeader)) {
      if (model != null && !model.getVersion().equals(etagHeader)) {
        return true;
      }
    }

    return false;
  }
}
